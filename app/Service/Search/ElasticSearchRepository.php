<?php

namespace App\Services\Search;

use Elasticsearch\Client;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Boolean;


class ElasticSearchRepository implements SearchInterface
{

    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    public function search( $query = '' , $operator = "AND", $minimumShouldMatch = "25%" )
    {
        $items = $this->searchOnElasticsearch($query , $operator , $minimumShouldMatch);

        return  $this->buildCollection($items);

    }

    private function searchOnElasticsearch( $query, $operator, $minimumShouldMatch ): array
    {

        $items = $this->elasticsearch->search([
            'body' => [
                'query' => [
                    'multi_match' => [
                        'fields' => [
                            'title^3' ,
                            'body_ru' ,
                            'body_uk' ,
                            'body_en' ,
                            'seo_title_ru^4',
                            'seo_title_uk^4',
                            'seo_title_en^4',
                            'seo_description_en^3',
                            'seo_description_uk^3',
                            'seo_description_ru^3',
                            'search_keywords^5'
                        ],
                        'query' => $query,
                        "fuzziness" => "AUTO",
                        "operator"  => $operator,
                        "minimum_should_match" => $minimumShouldMatch
                    ],
                ],
            ],
        ]);

        return $items;
    }

    public function isExists($id): Boolean
    {
        $params = [
            'index' => 'pages',
            'type' => '_doc',
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            [ 'match' => [ '_id' => $id ] ],
                        ]
                    ]
                ]
            ],
        ];


        return $this->elasticsearch->count($params)['count'];
    }

    private function buildCollection(array $items): Collection
    {

        $ids = Arr::pluck($items['hits']['hits'], '_id');

        if(!count($ids)) {
            return collect([]);
        }

        $idsOrdered = implode(',', $ids);

        return Page::whereIn('id', $ids)
            ->orderByRaw("FIELD(id, $idsOrdered)")
            ->get()
            ->sortByDesc('search_priority');
    }

}
