<?php


namespace App\Services\Search;


class PageIndexConfigurator
{
    public static function getConfigs(): array
    {
        return $params = [
            'index' => 'pages',
            'body' => [
                'settings' => [
                    'analysis' => [
                        'filter' => [
                            'shingle' => [
                                'type' => 'shingle',
                            ],
                            "russian_stop" => [
                                "type" => "stop",
                                "stopwords" => "_russian_",

                            ],
                            "russian_stemmer" => [
                                "type" => "stemmer",
                                "language" => "russian"
                            ],
                            "english_stemmer" => [
                                "type" => "stemmer",
                                "language" => "english"
                            ],
                            "my_phonetic_cyrillic" => [
                                "type" => "phonetic",
                                "encoder" => "beider_morse",
                                "rule_type" => "approx",
                                "name_type" => "generic",
                                "languageset" => ["cyrillic"]
                            ],
                            "my_phonetic_english" => [
                                "type" => "phonetic",
                                "encoder" => "beider_morse",
                                "rule_type" => "approx",
                                "name_type" => "generic",
                                "languageset" => ["english"]
                            ]
                        ],
                        'char_filter' => [
                            'html_strip',
                            'pre_negs' => [
                                'type' => 'pattern_replace',
                                'pattern' => '(\\w+)\\s+((?i:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint))\\b',
                                'replacement' => '~$1 $2'
                            ],
                            'post_negs' => [
                                'type' => 'pattern_replace',
                                'pattern' => '\\b((?i:never|no|nothing|nowhere|noone|none|not|havent|hasnt|hadnt|cant|couldnt|shouldnt|wont|wouldnt|dont|doesnt|didnt|isnt|arent|aint))\\s+(\\w+)',
                                'replacement' => '$1 ~$2'
                            ]
                        ],
                        'analyzer' => [
                            'body_index_analyzer_ru' => [
                                'type' => 'custom',
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase',
                                    'russian_stop',
                                    'russian_stemmer',
                                    'porter_stem',
                                    'trim',
                                    'my_phonetic_english',
                                    'my_phonetic_cyrillic',
                                ],
                                'char_filter' => ['pre_negs', 'post_negs', 'html_strip']
                            ],
                            'body_index_analyzer' => [
                                'type' => 'custom',
                                'tokenizer' => 'standard',
                                'filter' => ['lowercase',
                                    'stop',
                                    'english_stemmer',
                                    'porter_stem',
                                    'trim',
                                    'my_phonetic_english',
                                    'my_phonetic_cyrillic'
                                ],
                                'char_filter' => ['pre_negs', 'post_negs', 'html_strip']
                            ],
                        ]
                    ]
                ],
                'mappings' => [
                    'properties' => [
                        'title' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ],
                        'body_uk' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ],
                        'body_en' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer',
                        ],
                        'body_ru' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ],
                        'seo_title_ru' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ],
                        'seo_title_uk' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',

                        ],
                        'seo_title_en' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer',

                        ],
                        'seo_description_ru' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ],
                        'seo_description_uk' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ],
                        'seo_description_en' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer',
                        ],
                        'search_keywords' => [
                            'type' => 'text',
                            'analyzer' => 'body_index_analyzer_ru',
                            'copy_to' => 'combined',
                            'search_analyzer' => 'body_index_analyzer_ru',
                        ]

                    ]
                ]
            ]
        ];
    }
}
