<?php


namespace App\Service\Search;


use App\Model\TreePage;
use App\Services\Search\SearchInterface;

class EloquentSearchRepository implements SearchInterface
{
    public function search(string $query = '')
    {
        return TreePage::where('title', 'like', '%' . $query . '%')
            ->orWhere('description', 'like', '%' . $query . '%')
            ->orWhere('short_description', 'like', '%' . $query . '%')
            ->orWhere('body', 'like', '%' . $query. '%')
            ->get();
    }
}
