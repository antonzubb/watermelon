<?php

namespace App\Model;

use App\Traits\Searchable;
use App\Traits\SearchableBoot;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

class TreePage extends Model
{
    use NodeTrait;
    use SearchableBoot;
    use Searchable;

    protected $table = 'tree_pages';

    protected $guarded = ['id'];
}
