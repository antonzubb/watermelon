<?php


namespace App\Http\Controllers;


use App\Http\Resources\TreePageResource;

class TreePageController extends Controller
{
    public function index()
    {
        return TreePageResource::collection();
    }


}
