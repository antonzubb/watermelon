<?php

namespace App\Traits;

use App\Services\Search\ElasticSearchObserver;

trait SearchableBoot
{
    public static function bootSearchable()
    {
        static::observe(app(ElasticSearchObserver::class));
    }
}
