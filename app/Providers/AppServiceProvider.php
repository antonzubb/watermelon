<?php

namespace App\Providers;

use App\Service\Search\EloquentSearchRepository;
use App\Services\Search\ElasticSearchRepository;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;
use App\Services\Search\SearchInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerSearch();
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

    private function registerSearch()
    {
        $this->app->bind(SearchInterface::class, function ($app) {
            // This is useful in case we want to turn-off our
            // search cluster or when deploying the search
            // to a live, running application at first.

            if (! config('services.search.enabled')) {
                return new EloquentSearchRepository();
            }
            return new ElasticSearchRepository(
                $app->make(Client::class)
            );
        });

        $this->app->bind(Client::class, function ($app) {
            return ClientBuilder::create()
                ->setHosts($app['config']->get('services.search.hosts'))
                ->build();
        });
    }
}
